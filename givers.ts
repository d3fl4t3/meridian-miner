export const givers100 = [
    { address: 'EQDCW5gTc6wBzNWLMp3rgc5dBXP9mOgSwNJGMj-Ok1enGSNV', reward: 10000 },
    { address: 'EQDq6bNLH-dWPJUl7WwjV_6K9gmu-cBGR6zYXCSKwetomaer', reward: 10000 },
    { address: 'EQDMcJl4ntgzC9rlTSvxAzlTHFGFUhq8CJkt2k_dpe5t5fdl', reward: 10000 },
    { address: 'EQAfEHkM6goE4nBy3StDVvPPIYAEvCm6fMcH1uSqVrFOC3zG', reward: 10000 },
    { address: 'EQDAc11oMAT1UKOD6wqo6pE4ll_iQKt69aj5ly7tY2pQdkiD', reward: 10000 },
    { address: 'EQD0SK2wZTCiWuuqinzXuEzrgNZ4rn7I1t72rvG8FMMFkRST', reward: 10000 },
    { address: 'EQCTp-2GQFWge04pAozfGBVQubY7s-d5kiYnMA76EAtK6oX4', reward: 10000 },
    { address: 'EQBLuB8RN3QZlnDzp3il67vdoYw6NP00igggSsGdwqZz9ni3', reward: 10000 },
    { address: 'EQAjXF0tyaodu0DEmVGUBNe_LSoj9GT0n3ktgWbIi4a03tuB', reward: 10000 },
    { address: 'EQAZ1XjtrxSfmDEEQo4axo24ttcUr5MVEFxhcHSgRk8seobo', reward: 10000 }, // 100
]

export const givers1000 = [
    { address: 'EQA_AFgnTqLSuVQGaxGyZYmQceD8_rUSHHvdPujNBFdluL9F', reward: 1000 },
    { address: 'EQDgjQnT1YJhGvLk38IDUodwPa3HLJ38g703SgFpRp2kW8FV', reward: 1000 },
    { address: 'EQAApt_NS7LegD6gbn3RHuTViCkpmnK4DiKSr5ACDmLmlMsd', reward: 1000 },
    { address: 'EQD82Nmdun4-pA0K1jKdCVRwvV9SGVZrhQneLdARp7iKBKRp', reward: 1000 },
    { address: 'EQBl2-TPP9o9TSFjCQ4UsmJepU2hyrWFRjcDV1rd_Qoi-2KB', reward: 1000 },
    { address: 'EQA0-AR0AY2a6daW1wBGQkDcIaDStfbzc6WHeqxRrmY5lxlT', reward: 1000 },
    { address: 'EQDJn4m1ZYfHu2et0CCRPCGsEa6OlY6LtCDCPvXC63IXBzbM', reward: 1000 },
    { address: 'EQAh0X4RydrmGRO0vDvDkWi_ymIO0a5HlxzOp_-fuhgUea-m', reward: 1000 },
   { address: "EQCfX6neH7UfiychTWXmWfJxTxJ6sbdOoBJesyHz2RyCNYfq", reward: 1000, },
   {
     address: "EQCUoxbuxROf2GKmLJPvRjvd9JYTwXv5-yXA62FMN6X-KGsK",
     reward: 1000,
   }, // 10 000
 ];

